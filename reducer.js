'use strict'

const cloneDeep = require('lodash.clonedeep')

class Reducer {
  constructor (knownEvents) {
    this.__knownEvents = knownEvents
  }

  get (name) {
    const event = this.__knownEvents.find(event => event.name === name)

    if (!event) {
      throw new Error(`unknown event ${name}`)
    }

    return event
  }

  reduce (initialState, events) {
    return events.reduce(
      (state, { name, ...event }) => this.get(name).reduce(state, event),
      cloneDeep(initialState)
    )
  }
}

module.exports = Reducer
