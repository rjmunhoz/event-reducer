'use strict'

const Event = require('../../event')
const merge = require('lodash.merge')

const reduce = (state, { data, timestamp }) => {
  const { email } = data

  return merge({ ...state }, {
    email,
    updatedAt: timestamp
  })
}

module.exports = new Event({
  name: 'user-email-was-upserted',
  reduce
})
