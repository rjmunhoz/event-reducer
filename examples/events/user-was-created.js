'use strict'

const merge = require('lodash.merge')

const Event = require('../../event')

const reduce = (state, { data, timestamp, user }) => {
  const { id, firstName, lastName, email } = data

  return merge({ ...state }, {
    id,
    name: {
      first: firstName,
      last: lastName
    },
    email,
    createdAt: timestamp,
    updatedAt: timestamp
  })
}

module.exports = new Event({
  name: 'user-was-created',
  reduce
})
