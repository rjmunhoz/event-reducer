'use strict'

const ObjectId = require('bson-objectid')

const Reducer = require('../reducer')
const UserWasCreated = require('./events/user-was-created')
const UserEmailWasUpserted = require('./events/user-email-was-upserted')

const sleep = async (time) => new Promise((resolve) => { setTimeout(resolve, time) })

;(async () => {
  const events = []
  events.push(UserWasCreated.factory({
    id: ObjectId(),
    firstName: 'Rogério',
    lastName: 'Munhoz',
  }, { user: 'rjmunhoz', timestamp: new Date() }))

  await sleep(2000)

  events.push(UserEmailWasUpserted.factory({
    email: 'rogerio.j.munhoz@gmail.com'
  }, { user: 'rjmunhoz', timestamp: new Date() }))

  const reducer = new Reducer([UserWasCreated, UserEmailWasUpserted])

  const userState = reducer.reduce({
    id: null,
    name: {
      first: null,
      last: null
    },
    email: null,
    createdAt: null,
    updatedAt: null
  }, events)

  console.log('I reduced these:')
  console.log(JSON.stringify(events, null, 2))
  console.log('To this:')
  console.log(JSON.stringify(userState, null, 2))
})()
