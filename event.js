'use strict'

const ObjectId = require('bson-objectid')

const EventModel = function EventModel ({ name, reduce }) {
  const Event = function Event (properties) {
    return Object.freeze({ name, ...properties })
  }

  Object.defineProperty(Event, 'name', {
    value: name,
    writable: false
  })

  Event.reduce = reduce

  Event.factory = (data = { }, meta) => {
    return new Event({
      id: ObjectId(),
      data,
      timestamp: new Date(),
      ...meta
    })
  }

  return Event
}

module.exports = EventModel
